package com.rzx.godhandmator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import android.content.Context;

public class ParamsGenerator {

    private final static String FILE_PATH = "mnt/sdcard/GodHand/res/param.properties";
    private final static String BASE_PATH = "mnt/sdcard/GodHand/res/base_properties/";

    public static void generateParams(Context context, String imei)
            throws Exception {

        // 生成重点修改参数
        // String imei=getImei();
        String androidId = getAndroidId(imei);
        String mac = getMac(androidId);
        String bssid = getBssid(mac);
        String ssid = getSsid(bssid);
        String blueMac = getBluemac(ssid);
        String serial = getSerial(blueMac);
        int ipAddress = getIpAddress(blueMac);

        String imsi = getImsi();
        String simserial = getSimSerial();

        StringBuffer sb = new StringBuffer();
        sb.append("imei=").append(imei).append("\n");
        sb.append("androidid=").append(androidId).append("\n");
        sb.append("mac=").append(mac).append("\n");
        sb.append("bssid=").append(bssid).append("\n");
        sb.append("ssid=").append(ssid).append("\n");
        sb.append("bluemac=").append(blueMac).append("\n");
        sb.append("ro.build.serial=").append(serial).append("\n");
        sb.append("ipAddress=").append(ipAddress).append("\n");
        sb.append("imsi=").append(imsi).append("\n");
        sb.append("mcc=").append(imsi.substring(0, 3)).append("\n");
        sb.append("mnc=").append(imsi.substring(4, 5)).append("\n");
        sb.append("simserial=").append(simserial).append("\n");
        sb.append(getCell());

        // 随机抽取模块

        File basePath = new File(BASE_PATH);
        String[] files = basePath.list();
        if (files.length == 0) {
            return;
        }
        int i = (int) (Math.random() * files.length);
        InputStream is = new FileInputStream(BASE_PATH + files[i]);
        String base = inputStream2String(is);
        sb.append(base);

        // 写到sd卡上
        PrintWriter pw = new PrintWriter(new File(FILE_PATH));
        pw.write(sb.toString());
        pw.close();

    }

    private static String getImei() {
        // imei(15)=TAC(6)+FAC(2)+SNR(6)+SP(1)

        int[] imei_int = new int[15];
        imei_int[0] = 8;
        imei_int[1] = 6;
        imei_int[2] = (int) (Math.random() * 10);
        imei_int[3] = (int) (Math.random() * 10);
        imei_int[4] = (int) (Math.random() * 10);
        imei_int[5] = (int) (Math.random() * 10);
        imei_int[6] = 0;
        imei_int[7] = 2;
        imei_int[8] = (int) (Math.random() * 10);
        imei_int[9] = (int) (Math.random() * 10);
        imei_int[10] = (int) (Math.random() * 10);
        imei_int[11] = (int) (Math.random() * 10);
        imei_int[12] = (int) (Math.random() * 10);
        imei_int[13] = (int) (Math.random() * 10);
        imei_int[14] = (int) (Math.random() * 10);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < imei_int.length; i++) {
            sb.append(imei_int[i]);
        }
        return sb.toString();
    }

    private static String getAndroidId(String imei) {
        return MD5Encode(imei);
    }

    private static String getMac(String androidId) {
        String md = MD5Encode(androidId);
        StringBuffer sb = new StringBuffer();
        sb.append(md.substring(0, 2) + ":");
        sb.append(md.substring(2, 4) + ":");
        sb.append(md.substring(4, 6) + ":");
        sb.append(md.substring(6, 8) + ":");
        sb.append(md.substring(8, 10) + ":");
        sb.append(md.substring(10, 12));
        return sb.toString();
    }

    private static String getBssid(String mac) {
        String md = MD5Encode("rzx" + mac);
        StringBuffer sb = new StringBuffer();
        sb.append(md.substring(0, 2) + ":");
        sb.append(md.substring(2, 4) + ":");
        sb.append(md.substring(4, 6) + ":");
        sb.append(md.substring(6, 8) + ":");
        sb.append(md.substring(8, 10) + ":");
        sb.append(md.substring(10, 12));
        return sb.toString();
    }

    private static String getSsid(String bssid) {
        String md = MD5Encode(bssid);
        String[] strings = new String[] { "360wifi-", "TP-Link_", "TP-Link_",
                "ChinaNet-", "ZTE-", "D-link_" };
        int i = (int) (Math.random() * strings.length);
        StringBuffer sb = new StringBuffer();
        sb.append(strings[i]);
        sb.append(md.substring(0, 2));
        return sb.toString();
    }

    private static String getImsi() {
        // 460022535025034
        String[] title = { "46000", "46002", "46007" };
        int i = (int) (Math.random() * title.length);

        int r1 = 10000 + new java.util.Random().nextInt(90000);
        int r2 = 10000 + new java.util.Random().nextInt(90000);

        String imsi = title[i] + r1 + r2;
        return imsi;
    }

    private static int Luhn(String str) {
        int sum = 0;
        int tmp;
        for (int i = str.length() - 1, j = 0; i >= 0; i--, j++) {
            tmp = Integer.valueOf(String.valueOf(str.charAt(i)), 16);
            if (j % 2 == 0) {
                sum += tmp * 2 / 10 + tmp * 2 % 10;
            } else {
                sum += tmp;
            }
        }

        if (sum % 10 == 0) {
            return 0;
        }
        return 10 - sum % 10;
    }

    private static String getSimSerial() {
        String serialno = "";
        Random rand = new Random();

        // 中国移动
        serialno += "898600";
        // M ：号段，对应用户号码前3位 　　
        // 0：159 1：158 2：150 　　3：151 4-9：134-139 A：157 　　B：188 C：152 D：147
        // 　　D：147 E：187
        serialno += "0123456789ABCDE".charAt(rand.nextInt(15));
        // 用户号码第4位
        serialno += rand.nextInt(10);
        // 省编号
        int randInt = rand.nextInt(31);
        if (randInt < 10) {
            serialno += 0;
        }
        serialno += randInt;
        // 年号
        randInt = rand.nextInt(17);
        if (randInt < 10) {
            serialno += 0;
        }
        serialno += randInt;
        // SIM卡供应商代码
        // 0：雅斯拓1：GEMPLUS 2：武汉天喻3：江西捷德4：珠海东信和平
        // 5：大唐微电子通6：航天九州通7：北京握奇8：东方英卡
        // 9：北京华虹A ：上海柯斯
        serialno += "0123456789A".charAt(rand.nextInt(11));
        // 用户识别码
        serialno += 100000 + rand.nextInt(899999);
        // 校验码
        serialno += Luhn(serialno);

        return serialno;
    }

    private static String getCell() {
        String[] strings = new String[] { "9360_4782","10155_4422","9523_30926","9339_4810","9363_5471","9514_8875","9338_4332","9338_4202","9338_4280","9363_5263","10349_4233","8979_62069","9391_10308","10063_24403","9715_42096","9313_29042","10277_41732","10078_53038","9805_7043","10100_32217","9859_40006","10077_20047","10232_14112","10459_12026","9046_57827","10066_30561","9243_37511","9935_24539","9982_8900","9082_59373","9623_16981","10085_38372","9395_47317","10046_7235","10185_43345","9804_50051","10248_6501","9562_57515","9621_62131","9236_64323","9874_7555","9880_6699","9973_9647","9017_356","10246_37241","9872_40986","10349_4233","10319_54504","9631_47170","9796_20552","9286_20169" };
        int i = (int) (Math.random() * strings.length);
        String[] cell = strings[i].split("_");
        StringBuffer sb = new StringBuffer();
        sb.append("cell.lac=").append(cell[0]).append("\n");
        sb.append("cell.cid=").append(cell[1]).append("\n");
        return sb.toString();

    }

    private static String getBluemac(String ssid) {
        String md = MD5Encode(ssid);
        StringBuffer sb = new StringBuffer();
        sb.append(md.substring(0, 2) + ":");
        sb.append(md.substring(2, 4) + ":");
        sb.append(md.substring(4, 6) + ":");
        sb.append(md.substring(6, 8) + ":");
        sb.append(md.substring(8, 10) + ":");
        sb.append(md.substring(10, 12));
        return sb.toString();
    }

    private static String getSerial(String bluemac) {
        String md = MD5Encode(bluemac);
        return md.substring(0, 10);
    }

    private static int getIpAddress(String bluemac) {
        int n = Integer.valueOf(MD5Encode(bluemac).substring(8, 9), 16);
        int _1_168_192 = 108736;
        int random_1_168_192 = (int) (_1_168_192 + Math.pow(2, 24)
                * ((int) (Math.random() * n) + 2));
        return random_1_168_192;
    }

    private static String MD5Encode(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return bytesToHexString(md.digest(str.getBytes())).substring(8, 24);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static String bytesToHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer(bytes.length);
        String sTemp;
        for (int i = 0; i < bytes.length; i++) {
            sTemp = Integer.toHexString(0xFF & bytes[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toLowerCase());
        }
        return sb.toString();
    }

    public static String inputStream2String(InputStream in) throws IOException {
        StringBuffer out = new StringBuffer();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1;) {
            out.append(new String(b, 0, n));
        }
        return out.toString();
    }

}
