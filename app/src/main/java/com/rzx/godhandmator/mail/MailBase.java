package com.rzx.godhandmator.mail;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;

public abstract class MailBase {
	protected Properties properties = new Properties();
    protected String defaultConnTimeout = "10000";
    protected String defaultReadTimeout = "20000";

	public abstract void connect(String host, String port, String usr, String pwd) throws MessagingException;
}
