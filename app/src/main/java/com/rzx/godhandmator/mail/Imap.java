package com.rzx.godhandmator.mail;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;

public class Imap extends MailBase{
	private IMAPStore store;
	private IMAPFolder inbox;
	
	@Override
	public void connect(String host, String port, String usr, String pwd) throws MessagingException {
		// TODO Auto-generated method stub
		properties.put("mail.store.protocol", "imap");
		properties.put("mail.imap.port", port);
		properties.put("mail.imap.host", host);
        properties.put("mail.imap.connectiontimeout", defaultConnTimeout);
        properties.put("mail.imap.timeout", defaultReadTimeout);
		
		Session session = Session.getInstance(properties);

		session.setDebug(true);
        store = (IMAPStore) session.getStore("imap");
        store.connect(usr, pwd);
        inbox = (IMAPFolder) store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
	}

	public IMAPStore getStore() {
		return store;
	}
	
	public IMAPFolder getFolder(){
		return inbox;
	}
	
	public void close() {
		if (store != null) {
			try {
				store.close();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (inbox != null) {
			try {
				inbox.close(false);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
