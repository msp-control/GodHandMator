package com.rzx.godhandmator.utils;

import org.apache.http.util.EncodingUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2016/9/8.
 */
public class FileUtils {
    /**
     * 读一个文件内容。
     *
     * @param fileName 要读的文件名
     * @return 读取后的内容
     * @throws IOException
     */
    public static String readFile(String fileName) throws IOException{
        String res="";
        FileInputStream fin = new FileInputStream(fileName);
        int length = fin.available();
        byte [] buffer = new byte[length];
        fin.read(buffer);
        res = EncodingUtils.getString(buffer, "UTF-8");
        fin.close();
        return res;
    }

    /**
     * 覆盖方式写一个文件，如果文件不存在，将创建一个文件。
     *
     * @param fileName 文件名
     * @param writestr 文件内容
     * @throws IOException
     */
    public static void writeFile(String fileName, String writestr) throws IOException{
        FileOutputStream fout = new FileOutputStream(fileName);
        byte [] bytes = writestr.getBytes();
        fout.write(bytes);
        fout.close();
    }
}
