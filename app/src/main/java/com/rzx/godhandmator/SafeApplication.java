package com.rzx.godhandmator;

import android.app.Application;

/**
 * Created by Administrator on 2016/12/2.
 */
public class SafeApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CrashHandler.getInstance().init(getApplicationContext());
    }
}
